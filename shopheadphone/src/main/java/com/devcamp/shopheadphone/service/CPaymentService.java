package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CPayment;
import com.devcamp.shopheadphone.repository.IPaymentRepository;

@Service
public class CPaymentService {
    @Autowired
    IPaymentRepository paymentRepository;

    // Get All list Payment
    public List<CPayment> getAllPayments() {
        return paymentRepository.findAll();
    }

    // Get Payment by Id
    public CPayment getPaymentById(int id) {
        Optional<CPayment> paymentData = paymentRepository.findById(id);
        if (paymentData.isPresent()) {
            return paymentData.get();
        } else {
            return null;
        }
    }

    // Create new Payment
    public CPayment createNewPayment(CPayment pCPayment) {
        CPayment payment = new CPayment();
        payment.setAmmount(pCPayment.getAmmount());
        payment.setCheckNumber(pCPayment.getCheckNumber());
        payment.setCustomer(pCPayment.getCustomer());
        payment.setPaymentDate(pCPayment.getPaymentDate());
        return paymentRepository.save(payment);
    }

    // Update Payment
    public CPayment updatePayment(int id, CPayment pCPayment) {
        Optional<CPayment> paymentData = paymentRepository.findById(id);
        if (paymentData.isPresent()) {
            CPayment payment = paymentData.get();
            payment.setAmmount(pCPayment.getAmmount());
            payment.setCheckNumber(pCPayment.getCheckNumber());
            payment.setCustomer(pCPayment.getCustomer());
            payment.setPaymentDate(pCPayment.getPaymentDate());
            return paymentRepository.save(payment);
        } else {
            return null;
        }
    }

    // Delete Payment by Id
    public void deletePaymentById(int id) {
        paymentRepository.deleteById(id);
    }
}
