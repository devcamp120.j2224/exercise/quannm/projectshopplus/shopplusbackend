package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.COrderDetail;
import com.devcamp.shopheadphone.repository.IOrderDetailRepository;

@Service
public class COrderDetailService {
    @Autowired
    IOrderDetailRepository pIOrderDetailRepository;

    // Get All list orderDetail
    public List<COrderDetail> getAllOrderDetails() {
        return pIOrderDetailRepository.findAll();
    }

    // Get orderDetail by Id
    public COrderDetail getOrderDetailById(int id) {
        Optional<COrderDetail> orderDetailData = pIOrderDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
            return orderDetailData.get();
        } else {
            return null;
        }
    }

    // Create new orderDetail
    public COrderDetail createNewOrderDetail(COrderDetail pCOrderDetail) {
        COrderDetail orderDetail = new COrderDetail();
        orderDetail.setOrder(pCOrderDetail.getOrder());
        orderDetail.setPriceEach(pCOrderDetail.getPriceEach());
        orderDetail.setProduct(pCOrderDetail.getProduct());
        orderDetail.setQuantityOrder(pCOrderDetail.getQuantityOrder());
        return pIOrderDetailRepository.save(orderDetail);
    }

    // Update orderDetail
    public COrderDetail updateOrderDetail(int id, COrderDetail pCOrderDetail) {
        Optional<COrderDetail> orderDetailData = pIOrderDetailRepository.findById(id);
        if (orderDetailData.isPresent()) {
            COrderDetail orderDetail = orderDetailData.get();
            orderDetail.setOrder(pCOrderDetail.getOrder());
            orderDetail.setPriceEach(pCOrderDetail.getPriceEach());
            orderDetail.setProduct(pCOrderDetail.getProduct());
            orderDetail.setQuantityOrder(pCOrderDetail.getQuantityOrder());
            return pIOrderDetailRepository.save(orderDetail);
        } else {
            return null;
        }
    }

    // Delete orderDetail by Id
    public void deleteOrderDetailById(int id) {
        pIOrderDetailRepository.deleteById(id);
    }
}
