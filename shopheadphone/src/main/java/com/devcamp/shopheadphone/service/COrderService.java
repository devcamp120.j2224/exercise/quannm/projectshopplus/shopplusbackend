package com.devcamp.shopheadphone.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.COrder;
import com.devcamp.shopheadphone.repository.IOrderRepository;

@Service
public class COrderService {
    @Autowired
    IOrderRepository pOrderRepository;

    // Get All list Order
    public List<COrder> getAllOrders() {
        return pOrderRepository.findAll();
    }

    // Get Order by Id
    public COrder getOrderById(int id) {
        Optional<COrder> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            return orderData.get();
        } else {
            return null;
        }
    }

    // Create new Order
    public COrder createNewOrder(COrder pCOrder) {
        COrder order = new COrder();
        order.setComments(pCOrder.getComments());
        order.setCustomer(pCOrder.getCustomer());
        order.setOrderDate(new Date());
        order.setOrderDetails(pCOrder.getOrderDetails());
        order.setStatus(pCOrder.getStatus());
        return pOrderRepository.save(order);
    }

    // Update Order
    public COrder updateOrder(int id, COrder pCOrder) {
        Optional<COrder> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            order.setComments(pCOrder.getComments());
            order.setCustomer(pCOrder.getCustomer());
            order.setOrderDetails(pCOrder.getOrderDetails());
            order.setStatus(pCOrder.getStatus());
            return pOrderRepository.save(order);
        } else {
            return null;
        }
    }

    // Update Required Date
    public COrder updateRequiredDate(int id) {
        Optional<COrder> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            order.setRequiredDate(new Date());
            return pOrderRepository.save(order);
        } else {
            return null;
        }
    }

    // Update Shipped Date
    public COrder updateShippedDate(int id) {
        Optional<COrder> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            order.setShippedDate(new Date());
            return pOrderRepository.save(order);
        } else {
            return null;
        }
    }

    // Delete Order by Id
    public void deleteOrderById(int id) {
        pOrderRepository.deleteById(id);
    }
}
