package com.devcamp.shopheadphone.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CAdvise;
import com.devcamp.shopheadphone.repository.IAdviseRepository;

@Service
public class CAdviseService {
    @Autowired
    IAdviseRepository pAdviseRepository;

    // Get all advise
    public List<CAdvise> getAllAdvise() {
        return pAdviseRepository.findAll();
    }

    // Get advise By id
    public CAdvise getAdviseById(int id) {
        Optional<CAdvise> adviseData = pAdviseRepository.findById(id);
        if (adviseData.isPresent()) {
            return adviseData.get();
        } else {
            return null;
        }
    }

    // Create new advise
    public CAdvise createNewAdvise(CAdvise pAdvise) {
        CAdvise advise = new CAdvise();
        advise.setFullName(pAdvise.getFullName());
        advise.setMessage(pAdvise.getMessage());
        advise.setPhoneNum(pAdvise.getPhoneNum());
        advise.setEmail(pAdvise.getEmail());
        advise.setCreateDate(new Date());
        advise.setStatus("N");
        return pAdviseRepository.save(advise);
    }

    // Update advise by id
    public CAdvise updateAdvise(int id) {
        Optional<CAdvise> adviseData = pAdviseRepository.findById(id);
        if (adviseData.isPresent()) {
            CAdvise advise = adviseData.get();
            advise.setStatus("Y");
            return pAdviseRepository.save(advise);
        } else {
            return null;
        }
    }

    // Delete advise
    public void deleteAdvise(int id) {
        pAdviseRepository.deleteById(id);
    }
}
