package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CProductLine;
import com.devcamp.shopheadphone.repository.IProductLineRepository;

@Service
public class CProductLineService {
    @Autowired
    IProductLineRepository productLineRepository;

    // Get All list productLine
    public List<CProductLine> getAllProductLines() {
        return productLineRepository.findAll();
    }

    // Get productLine by Id
    public CProductLine getProductLineById(int id) {
        Optional<CProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            return productLineData.get();
        } else {
            return null;
        }
    }

    // Create new productLine
    public CProductLine createNewProductLine(CProductLine pCProductLine) {
        CProductLine productLine = new CProductLine();
        productLine.setDescription(pCProductLine.getDescription());
        productLine.setProductLine(pCProductLine.getProductLine());
        productLine.setProducts(pCProductLine.getProducts());
        return productLineRepository.save(productLine);
    }

    // Update productLine
    public CProductLine updateProductLine(int id, CProductLine pCProductLine) {
        Optional<CProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            CProductLine productLine = productLineData.get();
            productLine.setDescription(pCProductLine.getDescription());
            productLine.setProductLine(pCProductLine.getProductLine());
            productLine.setProducts(pCProductLine.getProducts());
            return productLineRepository.save(productLine);
        } else {
            return null;
        }
    }

    // Delete productLine by Id
    public void deleteProductLineById(int id) {
        productLineRepository.deleteById(id);
    }
}
