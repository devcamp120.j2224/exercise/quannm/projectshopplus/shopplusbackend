package com.devcamp.shopheadphone.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CProduct;
import com.devcamp.shopheadphone.repository.IProductRepository;

@Service
public class CProductService {
    @Autowired
    IProductRepository productRepository;

    // Get All list product
    public List<CProduct> getAllProducts() {
        return productRepository.findAll();
    }

    // Get All list product DESC
    public List<CProduct> getAllProductByPriceDESC() {
        return productRepository.getAllProductByPriceDESC();
    }

    // Get All list product ASC
    public List<CProduct> getAllProductByPriceASC() {
        return productRepository.getAllProductByPriceASC();
    }

    // Get product by Id
    public CProduct getProductById(int id) {
        Optional<CProduct> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            return productData.get();
        } else {
            return null;
        }
    }

    // Get product new
    public List<CProduct> getNewProduct() {
        return productRepository.getNewProduct();
    }

    // Get product best selling
    public List<CProduct> getProductBestSelling() {
        return productRepository.getProductBestSelling();
    }

    // Get product price less 1m5
    public List<CProduct> getProductPriceLess1M5() {
        return productRepository.getProductPriceLess1M5();
    }

    // Get product price less 1m5 DESC
    public List<CProduct> getProductPriceLess1M5DESC() {
        return productRepository.getProductPriceLess1M5DESC();
    }

    // Get product price less 1m5 ASC
    public List<CProduct> getProductPriceLess1M5ASC() {
        return productRepository.getProductPriceLess1M5ASC();
    }

    // Get product price more 1m5 and less 5m
    public List<CProduct> getProductPriceMore1M5AndLess5M() {
        return productRepository.getProductPriceMore1M5AndLess5M();
    }

    // Get product price more 1m5 and less 5m DESC
    public List<CProduct> getProductPriceMore1M5AndLess5MDESC() {
        return productRepository.getProductPriceMore1M5AndLess5MDESC();
    }

    // Get product price more 1m5 and less 5m ASC
    public List<CProduct> getProductPriceMore1M5AndLess5MASC() {
        return productRepository.getProductPriceMore1M5AndLess5MASC();
    }

    // Get product more 5m
    public List<CProduct> getProductPriceMore5M() {
        return productRepository.getProductPriceMore5M();
    }

    // Get product more 5m DESC
    public List<CProduct> getProductPriceMore5MDESC() {
        return productRepository.getProductPriceMore5MDESC();
    }

    // Get product more 5m ASC
    public List<CProduct> getProductPriceMore5MASC() {
        return productRepository.getProductPriceMore5MASC();
    }

    // Get product by vendor
    public List<CProduct> getProductByVendor(String nameVendor) {
        return productRepository.getProductByVendor(nameVendor);
    }

    // Get product by vendor DESC
    public List<CProduct> getProductByVendorDESC(String nameVendor) {
        return productRepository.getProductByVendorDESC(nameVendor);
    }

    // Get product by vendor ASC
    public List<CProduct> getProductByVendorASC(String nameVendor) {
        return productRepository.getProductByVendorASC(nameVendor);
    }

    // Create new product
    public CProduct createNewProduct(CProduct pCProduct) {
        CProduct product = new CProduct();
        product.setBuyPrice(pCProduct.getBuyPrice());
        product.setOrderDetails(pCProduct.getOrderDetails());
        product.setProductCode(pCProduct.getProductCode());
        product.setProductDescription(pCProduct.getProductDescription());
        product.setProductLine(pCProduct.getProductLine());
        product.setProductName(pCProduct.getProductName());
        product.setProductScale(pCProduct.getProductScale());
        product.setProductVendor(pCProduct.getProductVendor());
        product.setQuantityInStock(pCProduct.getQuantityInStock());
        product.setSold(pCProduct.getSold());
        product.setPhoto(pCProduct.getPhoto());
        product.setCreateDate(new Date());
        return productRepository.save(product);
    }

    // Update product
    public CProduct updateProduct(int id, CProduct pCProduct) {
        Optional<CProduct> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            CProduct product = productData.get();
            product.setBuyPrice(pCProduct.getBuyPrice());
            product.setOrderDetails(pCProduct.getOrderDetails());
            product.setProductCode(pCProduct.getProductCode());
            product.setProductDescription(pCProduct.getProductDescription());
            product.setProductLine(pCProduct.getProductLine());
            product.setProductName(pCProduct.getProductName());
            product.setProductScale(pCProduct.getProductScale());
            product.setProductVendor(pCProduct.getProductVendor());
            product.setQuantityInStock(pCProduct.getQuantityInStock());
            product.setSold(pCProduct.getSold());
            product.setPhoto(pCProduct.getPhoto());
            product.setUpdateDate(new Date());
            return productRepository.save(product);
        } else {
            return null;
        }
    }

    // Delete product by Id
    public void deleteProductById(int id) {
        productRepository.deleteById(id);
    }
}
