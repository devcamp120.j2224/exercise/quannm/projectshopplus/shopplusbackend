package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CCustomer;
import com.devcamp.shopheadphone.repository.ICustomerRepository;

@Service
public class CCustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    // Get All list Customer
    public List<CCustomer> getAllCustomers() {
        return pCustomerRepository.findAll();
    }

    // Get Customer by Id
    public CCustomer getCustomerById(int id) {
        Optional<CCustomer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            return customerData.get();
        } else {
            return null;
        }
    }

    // Create new Customer
    public CCustomer createNewCustomer(CCustomer pCCustomer) {
        CCustomer customer = new CCustomer();
        customer.setAddress(pCCustomer.getAddress());
        customer.setCity(pCCustomer.getCity());
        customer.setCountry(pCCustomer.getCountry());
        customer.setCreditLimit(pCCustomer.getCreditLimit());
        customer.setFirstName(pCCustomer.getFirstName());
        customer.setLastName(pCCustomer.getLastName());
        customer.setOrders(pCCustomer.getOrders());
        customer.setPayments(pCCustomer.getPayments());
        customer.setPhoneNumber(pCCustomer.getPhoneNumber());
        customer.setPostalCode(pCCustomer.getPostalCode());
        customer.setSalesRepEmployeeNumber(pCCustomer.getSalesRepEmployeeNumber());
        customer.setState(pCCustomer.getState());
        return pCustomerRepository.save(customer);
    }

    // Update Customer
    public CCustomer updateCustomer(int id, CCustomer pCCustomer) {
        Optional<CCustomer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            CCustomer customer = customerData.get();
            customer.setAddress(pCCustomer.getAddress());
            customer.setCity(pCCustomer.getCity());
            customer.setCountry(pCCustomer.getCountry());
            customer.setCreditLimit(pCCustomer.getCreditLimit());
            customer.setFirstName(pCCustomer.getFirstName());
            customer.setLastName(pCCustomer.getLastName());
            customer.setOrders(pCCustomer.getOrders());
            customer.setPayments(pCCustomer.getPayments());
            customer.setPhoneNumber(pCCustomer.getPhoneNumber());
            customer.setPostalCode(pCCustomer.getPostalCode());
            customer.setSalesRepEmployeeNumber(pCCustomer.getSalesRepEmployeeNumber());
            customer.setState(pCCustomer.getState());
            return pCustomerRepository.save(customer);
        } else {
            return null;
        }
    }

    // Delete Customer by Id
    public void deleteCustomerById(int id) {
        pCustomerRepository.deleteById(id);
    }
}
