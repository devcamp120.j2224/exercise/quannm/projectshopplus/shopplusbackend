package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.CEmployee;
import com.devcamp.shopheadphone.repository.IEmployeeRepository;

@Service
public class CEmployeeService {
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    // Get All list employees
    public List<CEmployee> getAllEmployees() {
        return pEmployeeRepository.findAll();
    }

    // Get Employee by Id
    public CEmployee getEmployeeById(int id) {
        Optional<CEmployee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return employeeData.get();
        } else {
            return null;
        }
    }

    // Create new employee
    public CEmployee createNewEmployee(CEmployee pCEmployee) {
        CEmployee employee = new CEmployee();
        employee.setEmail(pCEmployee.getEmail());
        employee.setExtension(pCEmployee.getExtension());
        employee.setFirstName(pCEmployee.getFirstName());
        employee.setJobTitle(pCEmployee.getJobTitle());
        employee.setLastName(pCEmployee.getLastName());
        employee.setOfficeCode(pCEmployee.getOfficeCode());
        employee.setReportTo(pCEmployee.getReportTo());
        return pEmployeeRepository.save(employee);
    }

    // Update employee
    public CEmployee updateEmployee(int id, CEmployee pCEmployee) {
        Optional<CEmployee> employeeData = pEmployeeRepository.findById(id);
        if (employeeData.isPresent()) {
            CEmployee employee = employeeData.get();
            employee.setEmail(pCEmployee.getEmail());
            employee.setExtension(pCEmployee.getExtension());
            employee.setFirstName(pCEmployee.getFirstName());
            employee.setJobTitle(pCEmployee.getJobTitle());
            employee.setLastName(pCEmployee.getLastName());
            employee.setOfficeCode(pCEmployee.getOfficeCode());
            employee.setReportTo(pCEmployee.getReportTo());
            return pEmployeeRepository.save(employee);
        } else {
            return null;
        }
    }

    // Delete employee by Id
    public void deleteEmployeeById(int id) {
        pEmployeeRepository.deleteById(id);
    }
}
