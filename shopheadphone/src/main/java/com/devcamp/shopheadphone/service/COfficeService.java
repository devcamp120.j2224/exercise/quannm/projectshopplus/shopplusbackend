package com.devcamp.shopheadphone.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopheadphone.model.COffice;
import com.devcamp.shopheadphone.repository.IOfficeRepository;

@Service
public class COfficeService {
    @Autowired
    IOfficeRepository pIOfficeRepository;

    // Get All list office
    public List<COffice> getAllOffices() {
        return pIOfficeRepository.findAll();
    }

    // Get office by Id
    public COffice getOfficeById(int id) {
        Optional<COffice> officeData = pIOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            return officeData.get();
        } else {
            return null;
        }
    }

    // Create new office
    public COffice createNewOffice(COffice pCOffice) {
        COffice office = new COffice();
        office.setAddressLine(pCOffice.getAddressLine());
        office.setCity(pCOffice.getCity());
        office.setCountry(pCOffice.getCountry());
        office.setPhone(pCOffice.getPhone());
        office.setState(pCOffice.getState());
        office.setTerritory(pCOffice.getTerritory());
        return pIOfficeRepository.save(office);
    }

    // Update office
    public COffice updateOffice(int id, COffice pCOffice) {
        Optional<COffice> officeData = pIOfficeRepository.findById(id);
        if (officeData.isPresent()) {
            COffice office = officeData.get();
            office.setAddressLine(pCOffice.getAddressLine());
            office.setCity(pCOffice.getCity());
            office.setCountry(pCOffice.getCountry());
            office.setPhone(pCOffice.getPhone());
            office.setState(pCOffice.getState());
            office.setTerritory(pCOffice.getTerritory());
            return pIOfficeRepository.save(office);
        } else {
            return null;
        }
    }

    // Delete office by Id
    public void deleteOfficeById(int id) {
        pIOfficeRepository.deleteById(id);
    }
}
