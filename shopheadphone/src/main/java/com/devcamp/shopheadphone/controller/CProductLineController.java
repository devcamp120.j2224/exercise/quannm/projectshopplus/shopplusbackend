package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CProductLine;
import com.devcamp.shopheadphone.service.CProductLineService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductLineController {
    @Autowired
    CProductLineService productLineService;

    // Get All ProductLine
    @GetMapping("/productLines")
    public ResponseEntity<List<CProductLine>> getAllProductLines() {
        try {
            return new ResponseEntity<List<CProductLine>>(productLineService.getAllProductLines(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get ProductLine By Id
    @GetMapping("/productLines/{id}")
    public ResponseEntity<CProductLine> getProductLineById(@PathVariable int id) {
        try {
            if (productLineService.getProductLineById(id) != null) {
                return new ResponseEntity<CProductLine>(productLineService.getProductLineById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new ProductLine
    @PostMapping("/productLines")
    public ResponseEntity<Object> createNewEmployee(@RequestBody CProductLine pCProductLine) {
        try {
            return new ResponseEntity<>(productLineService.createNewProductLine(pCProductLine), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put ProductLine by Id
    @PutMapping("/productLines/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id, @RequestBody CProductLine pCProductLine) {
        try {
            if (productLineService.updateProductLine(id, pCProductLine) != null) {
                return new ResponseEntity<>(productLineService.updateProductLine(id, pCProductLine), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete ProductLine by Id
    @DeleteMapping("/productLines/{id}")
    public ResponseEntity<Object> deleteProductLineById(@PathVariable int id) {
        try {
            productLineService.deleteProductLineById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
