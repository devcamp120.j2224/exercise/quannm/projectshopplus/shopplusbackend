package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CPayment;
import com.devcamp.shopheadphone.service.CPaymentService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CPaymentController {
    @Autowired
    CPaymentService pCPaymentService;

    // Get All Payment
    @GetMapping("/payments")
    public ResponseEntity<List<CPayment>> getAllPayments() {
        try {
            return new ResponseEntity<List<CPayment>>(pCPaymentService.getAllPayments(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Payment By Id
    @GetMapping("/payments/{id}")
    public ResponseEntity<CPayment> getPaymentById(@PathVariable int id) {
        try {
            if (pCPaymentService.getPaymentById(id) != null) {
                return new ResponseEntity<CPayment>(pCPaymentService.getPaymentById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Payment
    @PostMapping("/payments")
    public ResponseEntity<Object> createNewPayment(@RequestBody CPayment pCPayment) {
        try {
            return new ResponseEntity<>(pCPaymentService.createNewPayment(pCPayment), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Payment by Id
    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @RequestBody CPayment pCPayment) {
        try {
            if (pCPaymentService.updatePayment(id, pCPayment) != null) {
                return new ResponseEntity<>(pCPaymentService.updatePayment(id, pCPayment), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Payment by Id
    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Object> deletePaymentById(@PathVariable int id) {
        try {
            pCPaymentService.deletePaymentById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
