package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.COffice;
import com.devcamp.shopheadphone.service.COfficeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class COfficeController {
    @Autowired
    COfficeService pCOfficeService;

    // Get All Office
    @GetMapping("/offices")
    public ResponseEntity<List<COffice>> getAllOffices() {
        try {
            return new ResponseEntity<List<COffice>>(pCOfficeService.getAllOffices(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Office By Id
    @GetMapping("/offices/{id}")
    public ResponseEntity<COffice> getOfficeById(@PathVariable int id) {
        try {
            if (pCOfficeService.getOfficeById(id) != null) {
                return new ResponseEntity<COffice>(pCOfficeService.getOfficeById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Office
    @PostMapping("/offices")
    public ResponseEntity<Object> createNewOffice(@RequestBody COffice pCOffice) {
        try {
            return new ResponseEntity<>(pCOfficeService.createNewOffice(pCOffice), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Office by Id
    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id, @RequestBody COffice pCOffice) {
        try {
            if (pCOfficeService.updateOffice(id, pCOffice) != null) {
                return new ResponseEntity<>(pCOfficeService.updateOffice(id, pCOffice), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Office by Id
    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Object> deleteOfficeById(@PathVariable int id) {
        try {
            pCOfficeService.deleteOfficeById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
