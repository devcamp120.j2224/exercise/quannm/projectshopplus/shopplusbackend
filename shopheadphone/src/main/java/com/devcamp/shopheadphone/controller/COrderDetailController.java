package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.COrderDetail;
import com.devcamp.shopheadphone.service.COrderDetailService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class COrderDetailController {
    @Autowired
    COrderDetailService pCOrderDetailService;

    // Get All OrderDetail
    @GetMapping("/orderDetails")
    public ResponseEntity<List<COrderDetail>> getAllOrderDetails() {
        try {
            return new ResponseEntity<List<COrderDetail>>(pCOrderDetailService.getAllOrderDetails(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get OrderDetail By Id
    @GetMapping("/orderDetails/{id}")
    public ResponseEntity<COrderDetail> getOrderDetailById(@PathVariable int id) {
        try {
            if (pCOrderDetailService.getOrderDetailById(id) != null) {
                return new ResponseEntity<COrderDetail>(pCOrderDetailService.getOrderDetailById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new OrderDetail
    @PostMapping("/orderDetails")
    public ResponseEntity<Object> createNewOrderDetail(@RequestBody COrderDetail pCOrderDetail) {
        try {
            return new ResponseEntity<>(pCOrderDetailService.createNewOrderDetail(pCOrderDetail), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put OrderDetail by Id
    @PutMapping("/orderDetails/{id}")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable("id") int id, @RequestBody COrderDetail pCOrderDetail) {
        try {
            if (pCOrderDetailService.updateOrderDetail(id, pCOrderDetail) != null) {
                return new ResponseEntity<>(pCOrderDetailService.updateOrderDetail(id, pCOrderDetail), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete OrderDetail by Id
    @DeleteMapping("/orderDetails/{id}")
    public ResponseEntity<Object> deleteOrderDetailById(@PathVariable int id) {
        try {
            pCOrderDetailService.deleteOrderDetailById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
