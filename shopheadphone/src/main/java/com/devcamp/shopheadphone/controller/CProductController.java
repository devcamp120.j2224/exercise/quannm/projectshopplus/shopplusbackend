package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CProduct;
import com.devcamp.shopheadphone.service.CProductService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductController {
    @Autowired
    CProductService productService;

    // Get All Product
    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProducts() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getAllProducts(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get All Product price DESC
    @GetMapping("/products/DESC")
    public ResponseEntity<List<CProduct>> getAllProductByPriceDESC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getAllProductByPriceDESC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get All Product price ASC
    @GetMapping("/products/ASC")
    public ResponseEntity<List<CProduct>> getAllProductByPriceASC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getAllProductByPriceASC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product By Id
    @GetMapping("/products/{id}")
    public ResponseEntity<CProduct> getProductById(@PathVariable int id) {
        try {
            if (productService.getProductById(id) != null) {
                return new ResponseEntity<CProduct>(productService.getProductById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product new
    @GetMapping("/products/new")
    public ResponseEntity<List<CProduct>> getNewProduct() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getNewProduct(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product Best Selling
    @GetMapping("/products/bestSelling")
    public ResponseEntity<List<CProduct>> getProductBestSelling() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductBestSelling(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price less 1m5
    @GetMapping("/products/less1m5")
    public ResponseEntity<List<CProduct>> getProductPriceLess1M5() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceLess1M5(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price less 1m5 DESC
    @GetMapping("/products/less1m5/DESC")
    public ResponseEntity<List<CProduct>> getProductPriceLess1M5DESC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceLess1M5DESC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price less 1m5 ASC
    @GetMapping("/products/less1m5/ASC")
    public ResponseEntity<List<CProduct>> getProductPriceLess1M5ASC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceLess1M5ASC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 1m5 and less 5m
    @GetMapping("/products/more1m5AndLess5m")
    public ResponseEntity<List<CProduct>> getProductPriceMore1M5AndLess5M() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore1M5AndLess5M(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 1m5 and less 5m DESC
    @GetMapping("/products/more1m5AndLess5m/DESC")
    public ResponseEntity<List<CProduct>> getProductPriceMore1M5AndLess5MDESC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore1M5AndLess5MDESC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 1m5 and less 5m ASC
    @GetMapping("/products/more1m5AndLess5m/ASC")
    public ResponseEntity<List<CProduct>> getProductPriceMore1M5AndLess5MASC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore1M5AndLess5MASC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 5m
    @GetMapping("/products/more5m")
    public ResponseEntity<List<CProduct>> getProductPriceMore5M() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore5M(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 5m DESC
    @GetMapping("/products/more5m/DESC")
    public ResponseEntity<List<CProduct>> getProductPriceMore5MDESC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore5MDESC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product price more 5m ASC
    @GetMapping("/products/more5m/ASC")
    public ResponseEntity<List<CProduct>> getProductPriceMore5MASC() {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductPriceMore5MASC(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product by Vendor name
    @GetMapping("/products/vendor/{nameVendor}")
    public ResponseEntity<List<CProduct>> getProductByVendor(@PathVariable("nameVendor") String nameVendor) {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductByVendor(nameVendor), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product by Vendor name DESC
    @GetMapping("/products/vendor/DESC/{nameVendor}")
    public ResponseEntity<List<CProduct>> getProductByVendorDESC(@PathVariable("nameVendor") String nameVendor) {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductByVendorDESC(nameVendor), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Product by Vendor name ASC
    @GetMapping("/products/vendor/ASC/{nameVendor}")
    public ResponseEntity<List<CProduct>> getProductByVendorASC(@PathVariable("nameVendor") String nameVendor) {
        try {
            return new ResponseEntity<List<CProduct>>(productService.getProductByVendorASC(nameVendor), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Product
    @PostMapping("/products")
    public ResponseEntity<Object> createNewProduct(@RequestBody CProduct pCProduct) {
        try {
            return new ResponseEntity<>(productService.createNewProduct(pCProduct), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Product by Id
    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @RequestBody CProduct pCProduct) {
        try {
            if (productService.updateProduct(id, pCProduct) != null) {
                return new ResponseEntity<>(productService.updateProduct(id, pCProduct), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Product by Id
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable int id) {
        try {
            productService.deleteProductById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
