package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CEmployee;
import com.devcamp.shopheadphone.service.CEmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CEmployeeController {
    @Autowired
    CEmployeeService pCEmployeeService;

    // Get All Employee
    @GetMapping("/employees")
    public ResponseEntity<List<CEmployee>> getAllEmployees() {
        try {
            return new ResponseEntity<List<CEmployee>>(pCEmployeeService.getAllEmployees(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Employee By Id
    @GetMapping("/employees/{id}")
    public ResponseEntity<CEmployee> getEmployeeById(@PathVariable int id) {
        try {
            if (pCEmployeeService.getEmployeeById(id) != null) {
                return new ResponseEntity<CEmployee>(pCEmployeeService.getEmployeeById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Employee
    @PostMapping("/employees")
    public ResponseEntity<Object> createNewEmployee(@RequestBody CEmployee pCEmployee) {
        try {
            return new ResponseEntity<>(pCEmployeeService.createNewEmployee(pCEmployee), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Employee by Id
    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody CEmployee pCEmployee) {
        try {
            if (pCEmployeeService.updateEmployee(id, pCEmployee) != null) {
                return new ResponseEntity<>(pCEmployeeService.updateEmployee(id, pCEmployee), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Employee by Id
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable int id) {
        try {
            pCEmployeeService.deleteEmployeeById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
