package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CCustomer;
import com.devcamp.shopheadphone.service.CCustomerService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CCustomerController {
    @Autowired
    CCustomerService pCustomerService;

    // Get All Customer
    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers() {
        try {
            return new ResponseEntity<List<CCustomer>>(pCustomerService.getAllCustomers(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Customer By Id
    @GetMapping("/customers/{id}")
    public ResponseEntity<CCustomer> getCustomerById(@PathVariable int id) {
        try {
            if (pCustomerService.getCustomerById(id) != null) {
                return new ResponseEntity<CCustomer>(pCustomerService.getCustomerById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Customer
    @PostMapping("/customers")
    public ResponseEntity<Object> createNewCustomer(@RequestBody CCustomer pCustomer) {
        try {
            return new ResponseEntity<>(pCustomerService.createNewCustomer(pCustomer), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Customer by Id
    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @RequestBody CCustomer pCustomer) {
        try {
            if (pCustomerService.updateCustomer(id, pCustomer) != null) {
                return new ResponseEntity<>(pCustomerService.updateCustomer(id, pCustomer), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Customer by Id
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            pCustomerService.deleteCustomerById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
