package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.CAdvise;
import com.devcamp.shopheadphone.service.CAdviseService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CAdviseController {
    @Autowired
    CAdviseService pAdviseService;

    // Get All advise
    @GetMapping("/advise")
    public ResponseEntity<List<CAdvise>> getAllAdvise() {
        try {
            return new ResponseEntity<List<CAdvise>>(pAdviseService.getAllAdvise(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get advise By Id
    @GetMapping("/advise/{id}")
    public ResponseEntity<CAdvise> getAdviseById(@PathVariable int id) {
        try {
            if (pAdviseService.getAdviseById(id) != null) {
                return new ResponseEntity<CAdvise>(pAdviseService.getAdviseById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new advise
    @PostMapping("/advise")
    public ResponseEntity<Object> createNewAdvise(@RequestBody CAdvise pCAdvise) {
        try {
            return new ResponseEntity<>(pAdviseService.createNewAdvise(pCAdvise), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put advise by Id
    @PutMapping("/advise/{id}")
    public ResponseEntity<Object> updateAdvise(@PathVariable("id") int id) {
        try {
            if (pAdviseService.updateAdvise(id) != null) {
                return new ResponseEntity<>(pAdviseService.updateAdvise(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete advise by Id
    @DeleteMapping("/advise/{id}")
    public ResponseEntity<Object> deleteAdvise(@PathVariable int id) {
        try {
            pAdviseService.deleteAdvise(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
