package com.devcamp.shopheadphone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopheadphone.model.COrder;
import com.devcamp.shopheadphone.service.COrderService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class COrderController {
    @Autowired
    COrderService pCOrderService;

    // Get All Order
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders() {
        try {
            return new ResponseEntity<List<COrder>>(pCOrderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Order By Id
    @GetMapping("/orders/{id}")
    public ResponseEntity<COrder> getOrderById(@PathVariable int id) {
        try {
            if (pCOrderService.getOrderById(id) != null) {
                return new ResponseEntity<COrder>(pCOrderService.getOrderById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post new Order
    @PostMapping("/orders")
    public ResponseEntity<Object> createNewOrder(@RequestBody COrder pCOrder) {
        try {
            return new ResponseEntity<>(pCOrderService.createNewOrder(pCOrder), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Order by Id
    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") int id, @RequestBody COrder pCOrder) {
        try {
            if (pCOrderService.updateOrder(id, pCOrder) != null) {
                return new ResponseEntity<>(pCOrderService.updateOrder(id, pCOrder), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Order to required
    @PutMapping("/orders/required/{id}")
    public ResponseEntity<Object> updateRequiredDate(@PathVariable("id") int id) {
        try {
            if (pCOrderService.updateRequiredDate(id) != null) {
                return new ResponseEntity<>(pCOrderService.updateRequiredDate(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Order to shipped
    @PutMapping("/orders/shipped/{id}")
    public ResponseEntity<Object> updateShippedDate(@PathVariable("id") int id) {
        try {
            if (pCOrderService.updateShippedDate(id) != null) {
                return new ResponseEntity<>(pCOrderService.updateShippedDate(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Order by Id
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable int id) {
        try {
            pCOrderService.deleteOrderById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
