package com.devcamp.shopheadphone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopheadphoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopheadphoneApplication.class, args);
	}

}
