package com.devcamp.shopheadphone.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class CProductLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "product_line", unique = true)
    private String productLine;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "productLine")
    @JsonIgnore
    private List<CProduct> products;

    public CProductLine() {
    }

    public CProductLine(int id, String productLine, String description, List<CProduct> products) {
        this.id = id;
        this.productLine = productLine;
        this.description = description;
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CProduct> getProducts() {
        return products;
    }

    public void setProducts(List<CProduct> products) {
        this.products = products;
    }
}
