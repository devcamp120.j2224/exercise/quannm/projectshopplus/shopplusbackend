package com.devcamp.shopheadphone.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "order_details")
public class COrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private COrder order;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private CProduct product;

    @Column(name = "quantity_order")
    private int quantityOrder;

    @Column(name = "price_each")
    private BigDecimal priceEach;

    public COrderDetail() {
    }

    public COrderDetail(int id, int quantityOrder, BigDecimal priceEach) {
        this.id = id;
        this.quantityOrder = quantityOrder;
        this.priceEach = priceEach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public COrder getOrder() {
        return order;
    }

    public void setOrder(COrder order) {
        this.order = order;
    }

    public CProduct getProduct() {
        return product;
    }

    public void setProduct(CProduct product) {
        this.product = product;
    }

    public int getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(int quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    public BigDecimal getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(BigDecimal priceEach) {
        this.priceEach = priceEach;
    }
}
