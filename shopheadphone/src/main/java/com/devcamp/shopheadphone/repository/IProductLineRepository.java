package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CProductLine;

@Repository
public interface IProductLineRepository extends JpaRepository<CProductLine, Integer> {
    
}
