package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CEmployee;

@Repository
public interface IEmployeeRepository extends JpaRepository<CEmployee, Integer> {
    
}
