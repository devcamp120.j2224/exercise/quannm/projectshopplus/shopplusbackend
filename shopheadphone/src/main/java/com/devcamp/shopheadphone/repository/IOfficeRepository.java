package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.COffice;

@Repository
public interface IOfficeRepository extends JpaRepository<COffice, Integer> {
    
}
