package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.COrder;

@Repository
public interface IOrderRepository extends JpaRepository<COrder, Integer> {
    
}
