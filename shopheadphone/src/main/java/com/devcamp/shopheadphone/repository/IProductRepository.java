package com.devcamp.shopheadphone.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CProduct;

@Repository
public interface IProductRepository extends JpaRepository<CProduct, Integer> {
    @Query(value = "SELECT * FROM `products` ORDER BY `sold` DESC LIMIT 0,6;", nativeQuery = true)
    List<CProduct> getProductBestSelling();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` < 1500000;", nativeQuery = true)
    List<CProduct> getProductPriceLess1M5();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` < 1500000 ORDER BY `buy_price` DESC;", nativeQuery = true)
    List<CProduct> getProductPriceLess1M5DESC();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` < 1500000 ORDER BY `buy_price` ASC;", nativeQuery = true)
    List<CProduct> getProductPriceLess1M5ASC();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 1500000 AND `buy_price` < 5000000;", nativeQuery = true)
    List<CProduct> getProductPriceMore1M5AndLess5M();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 1500000 AND `buy_price` < 5000000 ORDER BY `buy_price` DESC;", nativeQuery = true)
    List<CProduct> getProductPriceMore1M5AndLess5MDESC();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 1500000 AND `buy_price` < 5000000 ORDER BY `buy_price` ASC;", nativeQuery = true)
    List<CProduct> getProductPriceMore1M5AndLess5MASC();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 5000000;", nativeQuery = true)
    List<CProduct> getProductPriceMore5M();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 5000000 ORDER BY `buy_price` DESC;", nativeQuery = true)
    List<CProduct> getProductPriceMore5MDESC();

    @Query(value = "SELECT * FROM `products` WHERE `buy_price` > 5000000 ORDER BY `buy_price` ASC;", nativeQuery = true)
    List<CProduct> getProductPriceMore5MASC();

    @Query(value = "SELECT * FROM `products` ORDER BY `buy_price` DESC;", nativeQuery = true)
    List<CProduct> getAllProductByPriceDESC();

    @Query(value = "SELECT * FROM `products` ORDER BY `buy_price` ASC;", nativeQuery = true)
    List<CProduct> getAllProductByPriceASC();

    @Query(value = "SELECT * FROM `products` ORDER BY `create_date` DESC LIMIT 0,9;", nativeQuery = true)
    List<CProduct> getNewProduct();

    @Query(value = "SELECT * FROM `products` WHERE `product_vendor` = :nameVendor", nativeQuery = true)
    List<CProduct> getProductByVendor(@Param("nameVendor") String nameVendor);

    @Query(value = "SELECT * FROM `products` WHERE `product_vendor` = :nameVendor ORDER BY `buy_price` DESC;", nativeQuery = true)
    List<CProduct> getProductByVendorDESC(@Param("nameVendor") String nameVendor);

    @Query(value = "SELECT * FROM `products` WHERE `product_vendor` = :nameVendor ORDER BY `buy_price` ASC;", nativeQuery = true)
    List<CProduct> getProductByVendorASC(@Param("nameVendor") String nameVendor);
}
