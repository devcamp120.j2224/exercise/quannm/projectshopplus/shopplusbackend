package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CAdvise;

@Repository
public interface IAdviseRepository extends JpaRepository<CAdvise, Integer> {
    
}
