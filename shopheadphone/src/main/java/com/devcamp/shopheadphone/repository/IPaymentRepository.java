package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CPayment;

@Repository
public interface IPaymentRepository extends JpaRepository<CPayment, Integer> {
    
}
