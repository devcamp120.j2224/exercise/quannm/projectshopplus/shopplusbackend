package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.CCustomer;

@Repository
public interface ICustomerRepository extends JpaRepository<CCustomer, Integer> {
    
}
