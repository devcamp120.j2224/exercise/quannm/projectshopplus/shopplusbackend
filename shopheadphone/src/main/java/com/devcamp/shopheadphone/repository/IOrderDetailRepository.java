package com.devcamp.shopheadphone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shopheadphone.model.COrderDetail;

@Repository
public interface IOrderDetailRepository extends JpaRepository<COrderDetail, Integer> {
    
}
